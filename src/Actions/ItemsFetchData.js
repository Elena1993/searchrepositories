export function itemsHasErrored(bool) {
    return {
        type: 'ITEMS_HAS_ERRORED',
        hasErrored: bool
    };
}

export function itemsIsLoading(bool) {
    return {
        type: 'ITEMS_IS_LOADING',
        isLoading: bool
    };
}
export function itemsFetchDataSuccess(items) {
    return dispatch =>{
        if(items.items[0].archive_url){
       dispatch(
        {type: 'ITEMS_FETCH_DATA_SUCCESS',
        payload:items}
        );} 
    }    
}
export function issuesFetchDataSuccess(items){
    return dispatch =>{
        dispatch({ type: 'ISSUES_FETCH_DATA_SUCCESS',
        payload:items
        })
    } 
}
export function itemsFetchData(url) {
    return (dispatch) => {
        dispatch(itemsIsLoading(true));
        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                dispatch(itemsIsLoading(false));
                console.log('responce', response)
                return response;
            })
            .then((response) => response.json()
            )
            .then((items) =>{console.log('uo',items);
            if(items.items){
            dispatch(itemsFetchDataSuccess(items))}
            else if(items){
                dispatch(issuesFetchDataSuccess(items))
            }
        
        })
            .catch(() => dispatch(itemsHasErrored(true)));
    };
}