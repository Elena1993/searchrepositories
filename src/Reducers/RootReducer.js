import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { items, itemsHasErrored, itemsIsLoading, issues} from './ItemsFetchDataReducer';

const RootReducer = combineReducers({
    form:formReducer,
    items,
    issues,
    itemsHasErrored,
    itemsIsLoading
})
export default RootReducer