import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import RootReducer from './Reducers/RootReducer'

const middleware = applyMiddleware(thunk, logger)
const store=createStore(
    RootReducer,
    middleware
)

export default store