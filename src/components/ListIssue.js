import React, {Component} from 'react';
import styled from 'styled-components';

const Text = styled.li`
:hover {
    transition: all 250ms ease-in-out;
    color: #00BAFF;  
}
`
const Title=styled.h3`
color: black;
word-break: break-all;
padding: 10px;
`
class ListIssue extends Component{
    render(){
        return(
            <div>
             <Title>Issues:</Title>
              {this.props.issues.map((issue)=>{ return(
               <div key={issue.id}>
                <Text>-{issue.title} </Text>
               </div>
            )})}
        </div>
        )
    }
}
export default ListIssue