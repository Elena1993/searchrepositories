import React, {Component} from 'react';
import {connect} from 'react-redux'
import {itemsFetchData} from '../Actions/ItemsFetchData'
import styled from 'styled-components';

const Title=styled.h3`
color: black;
word-break: break-all;
padding: 10px;
`
const Text = styled.li`
:hover {
    transition: all 250ms ease-in-out;
    color: #00BAFF;  
}
`
 class ListRepo extends Component{
    render(){
     return(
      <div>
       <Title>Repositories:</Title>
            {this.props.items.map((item) => {return(
                <div onClick={()=>{this.props.itemsFetchData(item.url+'/issues')}} key={item.id}>
                  <Text>-{item.name}</Text>
                </div>
        )})}            
      </div>
     )
    }
}
function mapStateToProps(state){
return{
    items:state.items
}}
export default connect(mapStateToProps, {itemsFetchData})(ListRepo)