import React, { Component } from 'react';
import SearchRepoForm from './SearchRepoForm'
import {connect} from 'react-redux'
import {itemsFetchData} from '../Actions/ItemsFetchData'
import ListRepo from './ListRepo'
import  ListIssue  from './ListIssue';
import styled from 'styled-components';

const Container = styled.div`
display:flex;
justify-content:space-around;
`
const ContainerForm= styled.div`
    width: 360px;
    height:100vh;
    margin: 20px;
    background-color: #9fe7a4;
    padding: 30px 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border-radius: 5.5px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    cursor: pointer;
    transition: 200ms ease-in-out;
    &:hover {
        box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
        transition: 200ms ease-in-out;
        transform: scale(1.10);
    }
`
const ContainerRepo = styled.div`
width: 360px;
height: auto;
margin: 20px;
background-color: #9fe7a4;
padding: 30px 20px;
display: flex;
flex-direction: column;
border-radius: 5.5px;
box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
cursor: pointer;
transition: 200ms ease-in-out;
&:hover {
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    transition: 200ms ease-in-out;
    transform: scale(1.10);
}
`
const ContainerIssues = styled.div`
width: 360px;
height: auto;
margin: 20px;
background-color: #9fe7a4;
padding: 30px 20px;
display: flex;
flex-direction: column;
border-radius: 5.5px;
box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
cursor: pointer;
transition: 200ms ease-in-out;
&:hover {
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    transition: 200ms ease-in-out;
    transform: scale(1.10);
}
`
class Root extends Component {
  render() {
    if (this.props.hasErrored) {
        return <p>Sorry! There was an error loading </p>;
    }

    if (this.props.isLoading) {
        return <p>Loading…</p>;
    }
    return (
      <>
        <Container>
            <ContainerForm>
                <SearchRepoForm onSubmit={this.handleSearchRepo}/>
            </ContainerForm>

            <ContainerRepo>
                <ListRepo/>
            </ContainerRepo>
   
            <ContainerIssues>
                <ListIssue issues={this.props.issues}/>
            </ContainerIssues>
        </Container>
     </>         
    )
  }
  handleSearchRepo=(value)=>{
      const lang=value.languages
      const repo=value.searchRepo
      const url=`https://api.github.com/search/repositories?q=+${repo}+language:${lang}`
      this.props.itemsFetchData(url)

        if(this.props.issues){
            this.props.issues.length=0
        }
    }
}
function mapStateToProps(state){
    return {
        hasErrored: state.itemsHasErrored,
        isLoading: state.itemsIsLoading,
        issues:state.issues   
    }
}
export default connect(mapStateToProps, {itemsFetchData})(Root)
