import React, { Component } from 'react'
import {reduxForm, Field} from 'redux-form'
import styled from 'styled-components';

const SubmitButton = styled.button`
    width: 114px;
    height: 43px;
    margin: 15px 18px 5px;
    text-transform: uppercase;
    cursor: pointer;
    transition: all 250 ease-in-out;
    border: none;
    box-shadow: 0 3px 3px rgba(0,0,0,0.16), 0 3px 3px rgba(0,0,0,0.23);
    &:hover {
        transition: all 250ms ease-in-out;
        color: #00BAFF;
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        background-color: #dbdbdb;
    }`

class SearchRepoForm extends Component{
    render(){
        const {handleSubmit} = this.props
        return(
            <div>
                <form onSubmit={handleSubmit}>
            <div>
                <Field
                    name="searchRepo"
                    component="input"
                    type="text"
                    placeholder="Название репозитория"
                />
            </div>
            <div>    
                <Field name="languages" component="select">
                    <option />
                    <option value="JavaScript">JavaScript</option>
                    <option value="Java">Java</option>
                    <option value="Assembler">Assembler</option>
                    <option value="Python">Python</option>
                    <option value="C">C</option>
                 </Field>
            </div>
            <div>
                <SubmitButton type="submit">Поиск </SubmitButton>
            </div>
                </form>
            </div>
        )
    }
}
export default reduxForm({
    form:'searchRepo'
})(SearchRepoForm)
